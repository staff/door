#include <stdio.h>
#include <sys/io.h>

#define BASE 0x378
#define DATA (base)
#define STATUS (base+1)

#define TRUE  1
#define FALSE 0

int base=BASE;

int openport(){
	if (ioperm(base, 3, 1)) {
		return FALSE;		
	} else {
		return TRUE;
	}
}

int closeport(){
	if (ioperm(base, 3, 0)) {
		return FALSE;
	} else {
		return TRUE;
	}
}		

void unlock(){
	outb(0x01,DATA);
}

void lock(){
	outb(0x00,DATA);
}
