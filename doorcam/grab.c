#include <stdio.h>
#include <fcntl.h>
#include <sys/ioctl.h>
#include <errno.h>
#include <string.h>
#include <unistd.h>
#include <asm/unistd.h>
#include <stdlib.h>
#include <linux/videodev.h>
#include <linux/videodev2.h>
#include <sys/mman.h>
#include <jpeglib.h>
#include <pthread.h>
#include <dmtx.h>
#include <sys/time.h>
#include <curl/curl.h>
#include <libavcodec/avcodec.h>

int verbose=0;
char *device = "/dev/video0";
int input = 0;
char *outdir = NULL;
int gwidth = 640;
int gheight = 480;
int gqual = 70;
int nbufs = 2;
int v4lmode = 2;
int gridsize = 100;
int web_port = 9191;
int auto_bright = 0;
int matrix_decode = 1;
int frame_interval = 250; /* 4fps */
int decode_interval = 750; /* mS */
int decode_timeout = 300; /* mS */
int init_bright = -1;
int pixfmt_out = V4L2_PIX_FMT_RGB24;
int background = 1;

#define next(a) (a>=nbufs-1?0:a+1)

pthread_t thread_list[10];

void ffmpeg_init();
int ffmpeg_decode(unsigned char *data_out, unsigned char *data_in, int len);

void help(char *name)
{
	fprintf(stderr,"Usage: %s [-v] [-d /dev/video0] [-i 0] [-o /data] [-w 640] [-h 480] [-q 70] [-1]\n",name);
	fprintf(stderr,"-v	verbose\n");
	fprintf(stderr,"-d s	input video device (%s)\n", device);
	fprintf(stderr,"-i n	video input source number (%d)\n", input);
	fprintf(stderr,"-o s	output directory for frames\n");
	fprintf(stderr,"-w n	capture width (%d)\n", gwidth);
	fprintf(stderr,"-h n	capture height (%d)\n", gheight);
	fprintf(stderr,"-q n	jpeg quality (%d%%)\n", gqual);
	fprintf(stderr,"-b n    number of frame buffers to use (%d)\n", nbufs);
	fprintf(stderr,"-g n    matrix search grid size(%d)\n", gridsize);
	fprintf(stderr,"-p n    port to run web server (%d)\n", web_port);
	fprintf(stderr,"-F n    frame grab interval (%dmS)\n", frame_interval);
	fprintf(stderr,"-D n    frame decode interval (%dmS)\n", decode_interval);
	fprintf(stderr,"-B n    set inital brightness\n");
	fprintf(stderr,"-a      toggle auto brightness (default %s)\n", auto_bright?"on":"off");
	fprintf(stderr,"-m      toggle matrix decode (default %s)\n", matrix_decode?"on":"off");
	fprintf(stderr,"-f      run in foreground\n");
	fprintf(stderr,"-1      Use V4L1 mode %s\n", v4lmode==1?"(default)":"");
	fprintf(stderr,"-2      Use V4L2 mode %s\n", v4lmode==2?"(default)":"");

	exit(1);
}

void vid_prep(int dev, int frame)
{
	struct video_mmap vid_mmap;
	vid_mmap.format = VIDEO_PALETTE_RGB24;
	vid_mmap.frame = frame;
	vid_mmap.width = gwidth;
	vid_mmap.height = gheight;
	if (ioctl(dev, VIDIOCMCAPTURE, &vid_mmap) == -1) {
		fprintf(stderr, "Error preparing buffer %d: %s\n", frame, strerror(errno));
	}
}

void vid_sync(int dev, int frame) 
{
	if (ioctl(dev, VIDIOCSYNC, &frame) == -1) {
		fprintf(stderr, "Error syncing buffer %d: %s\n", frame, strerror(errno));
	}
}

void put_jpeg(FILE *fp, unsigned char *image, int width, int height, int quality)
{
	int i,j;
	JSAMPROW y[16];
	struct jpeg_compress_struct cinfo;
        struct jpeg_error_mgr jerr;

	cinfo.err = jpeg_std_error(&jerr);  /* errors get written to stderr  */
        jpeg_create_compress (&cinfo);
        cinfo.image_width = gwidth;
        cinfo.image_height = gheight;
        cinfo.input_components = 3;
	cinfo.in_color_space = JCS_RGB;
        jpeg_set_defaults (&cinfo);

        jpeg_set_quality (&cinfo, quality, TRUE);
        cinfo.dct_method = JDCT_FASTEST;

	jpeg_stdio_dest(&cinfo, fp);
	jpeg_start_compress (&cinfo, TRUE);
	for (j=0;j<height;j+=16) {
                for (i=0;i<16;i++) {
                        y[i] = image + width*(i+j)*3;
                }
                jpeg_write_scanlines (&cinfo, y, 16);
        }

        jpeg_finish_compress (&cinfo);
        jpeg_destroy_compress (&cinfo);
}

void * run_v4l1(void *arg);
void * run_v4l2(void *arg);
void * run_matrix(void *arg);
void * run_web(void *arg);

void parse_matrix(unsigned char *img);
void queue_matrix(unsigned char *img);
void queue_web(unsigned char *img, unsigned int, int);

#define toggle(a) a?0:1

int main(int argc, char **argv)
{
	int opt;
	int dev;

	while ((opt=getopt(argc, argv, "vd:i:o:w:h:q:b:g:12p:amF:D:B:f"))!=-1) {
		switch (opt) {
			case 'v':
				verbose++;
				break;
			case 'a':
				auto_bright = toggle(auto_bright);
				break;
			case 'm':
				matrix_decode = toggle(matrix_decode);
				break;
			case 'd':
				device = strdup(optarg);
				break;
			case 'i':
				input = atoi(optarg);
				break;
			case 'o':
				if (*optarg==0) outdir=NULL; else
				outdir = strdup(optarg);
				break;
			case 'w':
				gwidth = atoi(optarg);
				break;
			case 'h':
				gheight = atoi(optarg);
				break;
			case 'q':
				gqual = atoi(optarg);
				break;
			case 'b':
				nbufs = atoi(optarg);
				break;
			case 'g':
				gridsize = atoi(optarg);
				break;
			case 'p':
				web_port = atoi(optarg);
				break;
			case '1':
				v4lmode = 1;
				break;
			case '2':
				v4lmode = 2;
				break;
			case 'F':
				frame_interval = atoi(optarg);
				break;
			case 'D':
				decode_interval = atoi(optarg);
				break;
			case 'B':
				init_bright = atoi(optarg);
				break;
			case 'f':
				background=0;
				break;
			default:
				help(argv[0]);
				break;
		}
	}

	/* open the device */
	if ((dev=open(device, O_RDWR))<0) {
		fprintf(stderr, "Error opening video device %s: %s\n", device, strerror(errno));
		return 2;
	}
	if (verbose) printf("Opened device %s\n", device);
	if (background) daemon(0,0);

	ffmpeg_init();

	if (v4lmode == 1) 
		pthread_create(&thread_list[0], NULL, run_v4l1, (void *)dev);
	else
		pthread_create(&thread_list[0], NULL, run_v4l2, (void *)dev);

	if (matrix_decode) pthread_create(&thread_list[1], NULL, run_matrix, NULL);
	pthread_create(&thread_list[2], NULL, run_web, NULL);
	pthread_join(thread_list[0], NULL);
	if (matrix_decode) pthread_join(thread_list[1], NULL);
	pthread_join(thread_list[2], NULL);
	return 0;
}


void * run_v4l1(void *arg)
{
	int dev = (int)arg;
	int i,n;
/*	struct video_channel vid_chnl; */
	struct video_mbuf vid_buf;
	struct video_picture vid_pic;
	unsigned char *map;
	int bright = 10000;

#if 0
	/* set the input number */
	vid_chnl.channel = input;
	if (ioctl(dev, VIDIOCGCHAN, &vid_chnl) == -1) {
		fprintf(stderr, "Error reading video device %s channel: %s\n", device, strerror(errno));
		return 1;
	}
	vid_chnl.channel = input;
	vid_chnl.norm = 0; /* PAL */
	if (ioctl(dev, VIDIOCSCHAN, &vid_chnl) == -1) {
		fprintf(stderr, "Error writing video device %s channel: %s\n", device, strerror(errno));
		return 1;
	}
	if (verbose) printf("Selecting input %d\n", vid_chnl.channel);
#endif

	/* select mmap mode */
	if (ioctl(dev, VIDIOCGMBUF, &vid_buf) == -1) {
		fprintf(stderr, "Error writing video device %s channel: %s\n", device, strerror(errno));
		return (void *)1;
	}

	if (verbose) printf("Device supports %d buffers, using %d\n", vid_buf.frames, nbufs);
	if (nbufs > vid_buf.frames) {
		fprintf(stderr, "Error tried to use too many buffers (%d) driver only supports %d\n", nbufs, vid_buf.frames);
		nbufs = vid_buf.frames;
	}

	map = mmap(0, vid_buf.size, PROT_READ|PROT_WRITE, MAP_SHARED, dev, 0);
	if (map == MAP_FAILED) {
		fprintf(stderr, "mmap failed: %s\n", strerror(errno));
		return (void *)1;
	}

	if (nbufs < 2) {
		fprintf(stderr, "Does not support even 2 frames.\n");
		return (void *)1;
	}

	if (verbose) {
		if (ioctl(dev, VIDIOCGPICT, &vid_pic) == -1) {
			fprintf(stderr, "Error reading video device %s image properties; %s\n", device, strerror(errno));
			return (void *)1;
		}
		printf("brightness: %d\n", vid_pic.brightness);
		printf("hue       : %d\n", vid_pic.hue);
		printf("colour    : %d\n", vid_pic.colour);
		printf("contrast  : %d\n", vid_pic.contrast);
		printf("whiteness : %d\n", vid_pic.whiteness);
		printf("depth     : %d\n", vid_pic.depth);
	}
	if (init_bright != -1) {
		if (ioctl(dev, VIDIOCGPICT, &vid_pic) == -1) {
			fprintf(stderr, "Error reading video device %s image properties; %s\n", device, strerror(errno));
			return (void *)1;
		}
		bright = init_bright;
		vid_pic.brightness = init_bright;
		printf("Setting inital brightness to %d\n", bright);
		if (ioctl(dev, VIDIOCSPICT, &vid_pic) == -1) {
			fprintf(stderr, "Error writign video device %s image properties; %s\n", device, strerror(errno));
			return (void *)1;
		}

	}

	i = 0;
	n = 0;
	vid_prep(dev, i);
	while (1) {
		vid_prep(dev, next(i) );
		vid_sync(dev, i);
		if (verbose) {printf("Grabbed %d (%d)    \r", n, i); fflush(stdout); }
		if (outdir) {
			char path[1024];
			FILE *fp;
			snprintf(path, sizeof(path), "%s/image%05d.jpg", outdir,n);
		       	fp = fopen(path, "w");

			if (fp == NULL) {
				fprintf(stderr, "Error opening %s: %s\n", path, strerror(errno));
			} else {
				put_jpeg(fp, map+vid_buf.offsets[i], gwidth, gheight, gqual);
				fclose(fp);
			}
		} else {
			/* read all the buffer bytes, just for the hell of it */
			unsigned long sum;
			unsigned char *p;
			int j, k;
			p = (unsigned char *)map + vid_buf.offsets[i];
			k = gwidth * gheight * 3;
			for (j=0;j<k;j++)
				sum += *p++;
		}
		if (matrix_decode) queue_matrix((unsigned char *)map + vid_buf.offsets[i]);
		queue_web((unsigned char *)map + vid_buf.offsets[i], 0, gwidth * gheight * 3);
		/* should we fix the brightness autobright */
		if (auto_bright && n % 10 == 0) {
			unsigned long hist[256];
			unsigned char *p = (unsigned char *)map + vid_buf.offsets[i];
			unsigned long count = 0, store=0, target=0;
			int j;
			int change=0;

			for (j=0;j<256;j++) hist[j]=0;
			for (i=j;j<(gwidth*gheight*3);j+=3) {
				int c = (p[0] + p[1] + p[2]) / 3;
				if (c < 0) c=0;
				if (c > 255) c=255;
				hist[c]++;
				p+=3;
				store++;
			}
			for (i=224;i<256;i++) count+= hist[i];
			target = (store * 1) / 100;

			if (ioctl(dev, VIDIOCGPICT, &vid_pic) == -1) {
				fprintf(stderr, "Error reading video device %s image properties; %s\n", device, strerror(errno));
				return (void *)1;
			}

			if (count <= 0) {
				/* brightness up */
				bright += 100;
				vid_pic.brightness = bright;
				if (verbose) printf("Brightness up to %d (%ld of %ld = %.1f%%)\n", vid_pic.brightness, count, target, (count*100)/(float)store);
				change=1;
			} else
			if (count > target) {
				/* brightness down */
				bright -= 100;
				vid_pic.brightness = bright;
				if (verbose) printf("Brightness down to %d (%ld of %ld = %.1f%%)\n", vid_pic.brightness, count, target, (count*100)/(float)store);
				change=1;
			}

			if (change)
			if (ioctl(dev, VIDIOCSPICT, &vid_pic) == -1) {
				fprintf(stderr, "Error writign video device %s image properties; %s\n", device, strerror(errno));
				return (void *)1;
			}
		}
		/* move along */
		n++;
		i = next(i);
	}
	return (void *)0;
}

void url_get(char *url, char *data)
{
	CURL *cl;
	char cerr[CURL_ERROR_SIZE];
	char buff[1024];
	char *clean;

	cl = curl_easy_init();
	clean = curl_easy_escape(cl, data, 0);
	snprintf(buff, sizeof(buff), url, clean);
	curl_free(clean);
	curl_easy_setopt(cl, CURLOPT_URL, buff);
	curl_easy_setopt(cl, CURLOPT_ERRORBUFFER, cerr);
	if (curl_easy_perform(cl))
		printf("URLGET Error: %s\n", cerr);
	curl_easy_cleanup(cl);
}

struct gotlist {
        char *text;
	struct gotlist *next;
};

	
void parse_matrix(unsigned char *img)
{
	/* look for barcodes */
	DmtxImage *image;
	DmtxDecode decode;
	DmtxRegion region;
	DmtxMessage *message;
	DmtxPixelLoc pMin,pMax;
	DmtxTime timeout;
	int count=0, errors=0;
	struct gotlist *found=NULL;
	struct gotlist *fp;

		
	pMin.X = 0;
	pMin.Y = 0;
	pMax.X = gwidth - 1;
	pMax.Y = gheight - 1;

	image = dmtxImageMalloc(gwidth, gheight);
	memcpy(image->pxl, img, gwidth*gheight*3);
	image->pageCount = 1;

	decode = dmtxDecodeStructInit(image);
	while (1) {
		/* waste at most 300mS looking for the first block */
		timeout = dmtxTimeAdd(dmtxTimeNow(), decode_timeout);
		region = dmtxDecodeFindNextRegion(&decode, &timeout);
		if (region.found != DMTX_REGION_FOUND) break;
		count++;

		message = dmtxDecodeMatrixRegion(image, &region, 1);
		if (message == NULL) {
			errors++;
			continue;
		}
		fp = found;
		while (fp != NULL) {
			if (strcmp((char *)message->output, fp->text)==0) break;
			fp=fp->next;
		}
		if (fp == NULL) {
			if (verbose) printf("Decoded Message: %s\n", message->output);
			url_get("http://sucs.org/doorknock.php?code=%s", (char *)message->output);
			fp = malloc(sizeof(struct gotlist));
			fp->text = strdup((char *)message->output);
			fp->next = found;
			found = fp;
		}
		dmtxMessageFree(&message);
	}
	dmtxDecodeStructDeInit(&decode);
	dmtxImageFree(&image);
	while (found != NULL) {
		fp = found->next;
		free(found->text);
		free(found);
		found = fp;
	}
}

void * run_v4l2(void *arg)
{
	int dev = (int)arg;
        void **framestore;
        int *framesize;
        struct v4l2_capability vcap;
/*        struct v4l2_input vin; */
        struct v4l2_format vformat;
        struct v4l2_requestbuffers vrbuf;
        struct v4l2_buffer vbuf;
/*        v4l2_std_id stdid; */
	int i,n;
	unsigned long hist[256];
	struct timeval now, last;
	int delay;

	if (ioctl(dev, VIDIOC_QUERYCAP, &vcap)) {
		fprintf(stderr, "%s Not a V4L2 device.\n", device);
		return (void *)1;
	}

	if (verbose) printf("%s: %s (%s)\n", vcap.driver, vcap.card, vcap.bus_info);
        if (!vcap.capabilities & V4L2_CAP_VIDEO_CAPTURE) {
                fprintf(stderr, "%s does not support capture.\n", device);
                return (void *)1;
	}
	if (!vcap.capabilities & V4L2_CAP_STREAMING) {
                fprintf(stderr, "%s does not support streaming mode.\n", device)
;
                return (void *)1;
        }

#if 0
	if (verbose) {
                vin.index = 0;
                printf("Available inputs:\n");
                while (ioctl(dev, VIDIOC_ENUMINPUT, &vin)==0) {
                        printf("%d '%s'\n", vin.index, vin.name);
                        vin.index++;
                }
        }

	vin.index = input;
	if (ioctl(dev, VIDIOC_ENUMINPUT, &vin)) {
		fprintf(stderr, "%s does not have an input %d.\n", device, input);
	} else {
		int chanis;
		ioctl(dev, VIDIOC_G_INPUT, &chanis);
		if (chanis != input) {
			if (verbose) printf("Changing from input %d to input %d\n", chanis, input);
			chanis = input;
			if (ioctl(dev, VIDIOC_S_INPUT, &chanis) || chanis != input) {
				fprintf(stderr, "Input %d not selectable, got %d instead.\n", input, chanis);
				input = chanis;
			}
		}else
			if (verbose) printf("Input already is %d\n", chanis);
	}

	/* just hard code pal */
	stdid = V4L2_STD_PAL_I;
	if (ioctl(dev, VIDIOC_S_STD, &stdid)) {
		fprintf(stderr, "Error selecting PAL-I: %s\n ", strerror(errno));
	}
#endif

	vformat.type = V4L2_BUF_TYPE_VIDEO_CAPTURE;
        vformat.fmt.pix.width = gwidth;
        vformat.fmt.pix.height = gheight;
        vformat.fmt.pix.pixelformat = V4L2_PIX_FMT_RGB24;
        vformat.fmt.pix.field = V4L2_FIELD_INTERLACED;
        vformat.fmt.pix.bytesperline = gwidth * 3;
        if (ioctl(dev, VIDIOC_S_FMT, &vformat)) {
                fprintf(stderr, "Setting format failed. aborting.\n");
                return (void *)1;
        }

	if (verbose) {
                printf("Capture size set to %dx%d bpl=%d size=%d format=%c%c%c%c\n",
                vformat.fmt.pix.width, vformat.fmt.pix.height,
                vformat.fmt.pix.bytesperline, vformat.fmt.pix.sizeimage,
                (vformat.fmt.pix.pixelformat & 0x000000FF ) >>  0,
                (vformat.fmt.pix.pixelformat & 0x0000FF00 ) >>  8,
                (vformat.fmt.pix.pixelformat & 0x00FF0000 ) >> 16,
                (vformat.fmt.pix.pixelformat & 0xFF000000 ) >> 24
                );
        }

	        /* mmap in the capture buffers */
        memset(&vrbuf, 0, sizeof(vrbuf));
        vrbuf.type = V4L2_BUF_TYPE_VIDEO_CAPTURE;
        vrbuf.memory = V4L2_MEMORY_MMAP;
        vrbuf.count = nbufs;
        if (ioctl(dev, VIDIOC_REQBUFS, &vrbuf)) {
                fprintf(stderr, "Requesting mmap buffers failed. aborting.\n");
                return (void *)1;
        }
        if (vrbuf.count < nbufs) {
                fprintf(stderr, "Couldnt get all %d requested buffers. only got %d.\n", nbufs, vrbuf.count);
		nbufs = vrbuf.count;
	}

	framestore = calloc(vrbuf.count, sizeof(void *));
        framesize = calloc(vrbuf.count, sizeof(int));
        for (i=0;i<vrbuf.count;i++) {
                int errn;
                memset(&vbuf, 0, sizeof(vbuf));
                vbuf.type = V4L2_BUF_TYPE_VIDEO_CAPTURE;
                vbuf.memory = V4L2_MEMORY_MMAP;
                vbuf.index = i;
                if (ioctl(dev, VIDIOC_QUERYBUF, &vbuf)) {
                        fprintf(stderr, "Error querying buffer %d of %d\n", i, vrbuf.count);
                }
                framestore[i] = mmap(NULL, vbuf.length, PROT_READ|PROT_WRITE, MAP_SHARED, dev, vbuf.m.offset);
                errn=errno;
                framesize[i] = vbuf.length;
                if (framestore[i] == MAP_FAILED) {
                        fprintf(stderr, "Failed mmap buffer %d of %d = %d bytes @ %x\n", i, vrbuf.count, vbuf.length, vbuf.m.offset);
                        fprintf(stderr, "Error: %s\n", strerror(errn));
                }
        }

	/* queue all buffers for receiving frames */
	if (verbose) printf("Queuing %d buffers for capture\n", vrbuf.count);
        for (i=0;i<vrbuf.count;i++) {
                memset(&vbuf, 0, sizeof(vbuf));
                vbuf.type = V4L2_BUF_TYPE_VIDEO_CAPTURE;
                vbuf.memory = V4L2_MEMORY_MMAP;
                vbuf.index = i;
                if (ioctl(dev, VIDIOC_QBUF, &vbuf)) {
			fprintf(stderr, "Error queueing buffer %d\n", i);
		}
        }

	i = 1;
	ioctl(dev, VIDIOC_STREAMON, &i);
	gettimeofday(&last, NULL);

	n=0;
	while (1) {
		 /* block and wait for next frame */
                memset(&vbuf, 0, sizeof(vbuf));
                vbuf.type = V4L2_BUF_TYPE_VIDEO_CAPTURE;
                vbuf.memory = V4L2_MEMORY_MMAP;
                ioctl(dev, VIDIOC_DQBUF, &vbuf);

		if (verbose) {printf("Grabbed %d (%d)     \r", n, vbuf.index); fflush(stdout); }
		if (outdir) {
			char path[1024];
			FILE *fp;
			snprintf(path, sizeof(path), "%s/image%05d.jpg", outdir,n);
		       	fp = fopen(path, "w");

			if (fp == NULL) {
				fprintf(stderr, "Error opening %s: %s\n", path, strerror(errno));
			} else {
				put_jpeg(fp, framestore[vbuf.index], vformat.fmt.pix.width, vformat.fmt.pix.height, gqual);
				fclose(fp);
			}
		} else {
#if 0
			/* read all the buffer bytes, just for the hell of it */
			unsigned long sum;
			unsigned char *p;
			int j, k;
			p = framestore[vbuf.index];
			k = vbuf.bytesused;
			for (j=0;j<k;j++)
				sum += *p++;
#endif
		}

		unsigned char *raw_image = framestore[vbuf.index];
		int raw_size = vbuf.bytesused;
		if (vformat.fmt.pix.pixelformat == V4L2_PIX_FMT_JPEG) {
			raw_image = malloc(gwidth*gheight*3);
			raw_size = ffmpeg_decode(raw_image, framestore[vbuf.index], vbuf.bytesused);
		}
		queue_matrix(raw_image);
		queue_web(framestore[vbuf.index], vformat.fmt.pix.pixelformat, vbuf.bytesused);
		n++;

		/* compute histogram */
		if (auto_bright) {
			struct v4l2_queryctrl qcontrol;
			struct v4l2_control control;
			unsigned char *p = raw_image;
			unsigned long count = 0, store=0, target=0;
			for (i=0;i<256;i++) hist[i]=0;
			p++;
			for (i=0;i<(gwidth*gheight*3);i+=3) {
				hist[*p]++;
				p+=3;
				store++;
			}
			for (i=240;i<256;i++) count+= hist[i];

			qcontrol.id = V4L2_CID_BRIGHTNESS;
			ioctl(dev, VIDIOC_QUERYCTRL, &qcontrol);
			control.id = V4L2_CID_BRIGHTNESS;
			ioctl(dev, VIDIOC_G_CTRL, &control);

			target = (store * 1) / 1000;

			if (count <= 0) {
				/* brightness up */
				if (control.value < qcontrol.maximum) {
					control.value += qcontrol.step;
					ioctl(dev, VIDIOC_S_CTRL, &control);
				}
				printf("Brightness up to %d (%d-%d) (%ld of %ld = %.1f%%)\n", control.value, qcontrol.minimum, qcontrol.maximum, count, target, 0.0);
			} else
			if (count > target) {
				/* brightness down */
				if (control.value > qcontrol.minimum && control.value > 0) {
					control.value -= qcontrol.step;
					ioctl(dev, VIDIOC_S_CTRL, &control);
				}
				printf("Brightness down to %d (%d-%d) (%ld of %ld = %.1f%%)\n", control.value, qcontrol.minimum, qcontrol.maximum, count, target, (count*100)/(float)store);
			} else {
				/*printf("Leave at %d (%d-%d) (%.1f%%)\n", control.value, qcontrol.minimum, qcontrol.maximum,  (count*100)/(float)store); */
			}
		}

		/* limit framerate */
		gettimeofday(&now, NULL);
		delay = ((now.tv_sec - last.tv_sec) * 1000000) + (now.tv_usec - last.tv_usec);
		last = now;
		if (delay < frame_interval*1000) {
			delay = (frame_interval * 1000) - delay;
			usleep( delay );
		}

		/* queue it again */
		ioctl(dev, VIDIOC_QBUF, &vbuf);

		/* if we had to decode it, release it */
		if (raw_image != framestore[vbuf.index]) {
			free(raw_image);
			raw_image = framestore[vbuf.index];
		}
	}
}

static unsigned char * matrix_img = NULL;
static pthread_cond_t  matrix_lock;

void * run_matrix(void *arg)
{
	pthread_mutex_t useless;
	pthread_mutex_init(&useless, NULL);
	pthread_cond_init(&matrix_lock, NULL);
	do {
		if (matrix_img != NULL) {
			parse_matrix(matrix_img);
			free(matrix_img);
			matrix_img = NULL;
		}
		pthread_mutex_lock(&useless);
		pthread_cond_wait(&matrix_lock, &useless);
		pthread_mutex_unlock(&useless);
	} while (1);
}

void queue_matrix(unsigned char *img)
{
	static struct timeval last;
	struct timeval now;
	int diff;

	gettimeofday(&now, NULL);
	if (last.tv_sec == 0)
		diff = decode_interval;
	else
		diff = ((now.tv_sec - last.tv_sec) * 1000) + ((now.tv_usec - last.tv_usec) / 1000);
	if (diff >= decode_interval && matrix_img == NULL) {
		matrix_img = malloc(gwidth*gheight*3);
		memcpy(matrix_img, img, gwidth*gheight*3);
		pthread_cond_signal(&matrix_lock);
		last = now;
	}
}

void ffmpeg_init()
{
	avcodec_init();
	avcodec_register_all();
}

/* do jpeg decode and format shift using libavcodec */
int ffmpeg_decode(unsigned char *data_out, unsigned char *data_in, int len)
{
	AVCodec *codec;
	AVCodecContext *c;
	AVFrame *picture;
	AVPicture outpic;
	int outlen = 0;
	int done;
	int fsize = gwidth * gheight;

	codec = avcodec_find_decoder(CODEC_ID_MJPEG);
	if (!codec) {
		printf("failed to load decoder");
		return 0;
	}
	c = avcodec_alloc_context();
	c->width = gwidth;
	c->height = gheight;

	picture = avcodec_alloc_frame();
	if (avcodec_open(c, codec) < 0) {
		err("failed to open decoder");
		return 0;
	}
	outlen = avcodec_decode_video(c, picture, &done, data_in, len);
#ifdef WITH_FILEIN
	fprintf(stderr, "size %dx%d fmt=0x%X used=%d/%d done=%d\n", 
		c->width, c->height, c->pix_fmt, outlen, len, done);
	fprintf(stderr, "data 0:%p 1:%p 2:%p\n", picture->data[0], picture->data[1], picture->data[2]);
	fprintf(stderr, "linesize 0:%d 1:%d 2:%d\n", picture->linesize[0], picture->linesize[1], picture->linesize[2]);
#endif
	if (outlen < 0  || done==0 ) {
		err("jpeg decode failed used=%d/%d done=%d", outlen, len, done);
		avcodec_close(c);
		av_free(picture);
		av_free(c);
		return 0;
	}
	outlen = 0;
	if (pixfmt_out == V4L2_PIX_FMT_RGB24) {
		outpic.data[0] = data_out;
		outpic.data[1] = NULL;
		outpic.data[2] = NULL;
		outpic.linesize[0] = gwidth * 3;
		if (img_convert(&outpic, PIX_FMT_RGB24, (AVPicture *)picture, c->pix_fmt, gwidth, gheight))
			err("format convert to yuyv failed");
		outlen = fsize*3;
	} else
	if (pixfmt_out == V4L2_PIX_FMT_YUYV) {
		outpic.data[0] = data_out;
		outpic.data[1] = NULL;
		outpic.data[2] = NULL;
		outpic.linesize[0] = gwidth * 2;
		if (img_convert(&outpic, PIX_FMT_YUYV422, (AVPicture *)picture, c->pix_fmt, gwidth, gheight)) 
			err("format convert to yuyv failed");
		outlen = fsize*2;
	} else
	if (pixfmt_out == V4L2_PIX_FMT_YUV420) {
#if 0
		uint8_t *p = data_out;
		memcpy(p, picture->data[0], fsize);
		p += fsize;
		memcpy(p, picture->data[1], fsize/4);
		p += fsize/4;
		memcpy(p, picture->data[2], fsize/4);
		outlen = fsize + fsize/2;
#else
		outpic.data[0] = data_out;
		outpic.data[1] = outpic.data[0] + fsize;
		outpic.data[2] = outpic.data[1] + fsize/4;
		outpic.linesize[0] = gwidth;
		outpic.linesize[1] = gwidth/2;
		outpic.linesize[2] = gwidth/2;
		//420 yu12
		if (img_convert(&outpic, PIX_FMT_YUV420P, (AVPicture *)picture, c->pix_fmt, gwidth, gheight)) 
			err("image convert to 420p failed");
#endif
		outlen = fsize + fsize/2;
	}else {
		err("unrecognised format in decode");
		outlen = 0;
	}
	avcodec_close(c);
	av_free(picture);
	av_free(c);
	return outlen;
}
