#include <stdio.h>
#include <fcntl.h>
#include <sys/ioctl.h>
#include <errno.h>
#include <string.h>
#include <unistd.h>
#include <asm/unistd.h>
#include <stdlib.h>
#include <libv4l1-videodev.h>
#include <linux/videodev2.h>
#include <sys/mman.h>
#include <jpeglib.h>
#include <pthread.h>
#include <dmtx.h>
#include <linux/futex.h>
#include <sys/time.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <netinet/tcp.h>
#include <signal.h>

extern int verbose;
extern int gwidth;
extern int gheight;
extern int web_port;


#define BOUNDARY "DataMatrixBoundaryString"

#define SERVER "Server: DataMatrix/0.1\r\nMax-Age: 0\r\nExpires: 0\r\nCache-Control: no-cache, private\r\nPragma: no-cache\r\n"

#define max(a,b) (a>b?a:b)

struct block {
	unsigned char *buff;
	int size;
	int sent;
	struct block *next;
};

struct user {
	int sock;
	struct block *block_list;
	struct user *next;
	enum { REQUEST, IMAGEONCE, IMAGESTREAM, DONE } status;
	char inbuf[4096];
	int inbuf_used;
	int inbuf_size;
};

struct user *user_list = NULL;

int futex(int *fut, int op, int val, struct timespec *rel);
void queue_web(unsigned char *img, unsigned int, int);
void parse_request(struct user *u);
void queue_block(struct user *u, void *data, int size);


typedef struct {
	struct jpeg_destination_mgr dest;
	JOCTET *buff;
	int size;
} my_destination;

void webput_init_destination (j_compress_ptr cinfo) {
	my_destination *jmgr = (my_destination *)cinfo->dest;

	jmgr->size = 32768;
	jmgr->buff = malloc(jmgr->size);
	jmgr->dest.next_output_byte = jmgr->buff;
	jmgr->dest.free_in_buffer = jmgr->size;
}

boolean webput_empty_output_buffer(j_compress_ptr cinfo) {
	my_destination *jmgr = (my_destination *)cinfo->dest;
	int len = jmgr->size - jmgr->dest.free_in_buffer;

	if (jmgr->dest.free_in_buffer < 4096) {
		jmgr->size *= 2;
		jmgr->buff = realloc(jmgr->buff, jmgr->size);
		jmgr->dest.free_in_buffer = jmgr->size - len;
		jmgr->dest.next_output_byte = jmgr->buff + len;
	}
	return TRUE;
}

void webput_term_destination (j_compress_ptr cinfo) {
}


unsigned char * webput_jpeg(int *imgsize, unsigned char *image, int width, int height, int quality)
{
	int i,j;
	JSAMPROW y[16];
	struct jpeg_compress_struct cinfo;
        struct jpeg_error_mgr jerr;
	my_destination jmgr;

	cinfo.err = jpeg_std_error(&jerr);  /* errors get written to stderr  */
        jpeg_create_compress (&cinfo);
        cinfo.image_width = width;
        cinfo.image_height = height;
        cinfo.input_components = 3;
	cinfo.in_color_space = JCS_RGB;
        jpeg_set_defaults (&cinfo);

        jpeg_set_quality (&cinfo, quality, TRUE);
        cinfo.dct_method = JDCT_FASTEST;

	/*jpeg_stdio_dest(&cinfo, fp); */
	jmgr.dest.init_destination = webput_init_destination;
	jmgr.dest.empty_output_buffer = webput_empty_output_buffer;
	jmgr.dest.term_destination = webput_term_destination;
	cinfo.dest = (struct jpeg_destination_mgr *)&jmgr;

	jpeg_start_compress (&cinfo, TRUE);
	for (j=0;j<height;j+=16) {
                for (i=0;i<16;i++) {
                        y[i] = image + width*(i+j)*3;
                }
                jpeg_write_scanlines (&cinfo, y, 16);
        }

        jpeg_finish_compress (&cinfo);
	*imgsize = jmgr.size - jmgr.dest.free_in_buffer;
        jpeg_destroy_compress (&cinfo);
	return jmgr.buff;
}


unsigned char * web_img = NULL;
unsigned int    web_img_fmt = 0;
unsigned int    web_img_size = 0;

void queue_block(struct user *u, void *data, int size)
{
	struct block *new, *end;
	new = malloc(sizeof(struct block));
	new->size = size;
	new->sent = 0;
	new->buff = malloc(size);
	new->next = NULL;
	memcpy(new->buff, data, size);

	if (u->block_list == NULL) {
		u->block_list = new;
	} else 
	{
		end = u->block_list;
		while (end->next != NULL) end=end->next;
		end->next = new;
	}
}

void purge_block(struct user *u)
{
	struct block *b, *n;
	b = u->block_list;
	u->block_list = NULL;
	while (b != NULL) {
		n = b->next;
		free(b->buff);
		free(b);
		b = n;
	}
}

void * run_web(void *arg)
{
	int mainsock;
	struct sockaddr_in sa;
	int i;

	signal(SIGPIPE,SIG_IGN);
	mainsock = socket(PF_INET, SOCK_STREAM, 0);
	i=1;
	setsockopt(mainsock, SOL_SOCKET, SO_REUSEADDR, &i, sizeof(i));

	sa.sin_family = AF_INET;
	sa.sin_port = htons(web_port);
	sa.sin_addr.s_addr = INADDR_ANY;
	if (bind(mainsock, (struct sockaddr *)&sa, sizeof(struct sockaddr_in))==-1) {
		fprintf(stderr, "bind port %d: %s\n", web_port, strerror(errno));
		close(mainsock);
		return (int *)0;
	}
	listen(mainsock, 1);

	do {
		fd_set fdread, fdwrite, fdex;
		struct user *u;
		struct timeval timeout;
		int smax;

		FD_ZERO(&fdread);
		FD_ZERO(&fdwrite);
		FD_ZERO(&fdex);
		FD_SET(mainsock, &fdread);
		FD_SET(mainsock, &fdex);
		smax = mainsock;
		u = user_list;
		while (u!=NULL) {
			if (u->status == REQUEST) 
				FD_SET(u->sock, &fdread);
			if (u->block_list != NULL) 
				FD_SET(u->sock, &fdwrite);
			FD_SET(u->sock, &fdex);
			smax = max(smax, u->sock);
			u = u->next;
		}
		timeout.tv_sec = 0;
		timeout.tv_usec = 1000000 / 50;
		select(smax+1, &fdread, &fdwrite, &fdex, &timeout);

		u = user_list;
		while (u!=NULL) {
			if (FD_ISSET(u->sock, &fdread)) {
				int n;
				n = read(u->sock, &(u->inbuf[u->inbuf_used]), u->inbuf_size - u->inbuf_used);
				if ( n <= 0 )
					FD_SET(u->sock, &fdex);
				else {
					u->inbuf_used += n;
					parse_request(u);
				}
			}
			if (u->block_list && FD_ISSET(u->sock, &fdwrite)) {
				struct block *old = u->block_list;
				if (write(u->sock, old->buff, old->size) == -1)
					FD_SET(u->sock, &fdex);
				u->block_list = old->next;
				free(old->buff);
				free(old);
			}
			if (u->block_list == NULL && u->status == DONE) {
				/* its finished, close and clean up */
				FD_SET(u->sock, &fdex);
			}
			if (FD_ISSET(u->sock, &fdex)) {
				close(u->sock);
				if (u == user_list) {
					user_list = u->next;
					purge_block(u);
					free(u);
					u = user_list;
				} else {
					struct user *oldu = user_list;
					while (oldu->next != u) oldu=oldu->next;
					oldu->next = u->next;
					purge_block(u);
					free(u);
					u = oldu->next;
				}
				if (verbose) printf("Web: Drop user\n");
				continue;
			}
			u=u->next;
		}
		if (FD_ISSET(mainsock, &fdread)) {
			struct user *u;
			socklen_t sl;
			u = malloc(sizeof(struct user));
			sl = sizeof(struct sockaddr_in);
			u->sock = accept(mainsock,(struct sockaddr *)&sa, &sl);
			u->block_list = NULL;
			u->next = user_list;
			u->status = REQUEST;
			u->inbuf_used = 0;
			u->inbuf_size = 4096;
			user_list = u;
			if (verbose) printf("Web: Add user\n");
		}

		if (web_img != NULL) {
			char text[1024];
			unsigned char *newimg;
			int size;
		
			if (web_img_fmt == V4L2_PIX_FMT_JPEG) {
				newimg = web_img;
				size = web_img_size;
			} else	
				newimg = webput_jpeg(&size, web_img, gwidth, gheight, 70);
			snprintf(text,sizeof(text),"Content-type: image/jpeg\r\nContent-Length: %d\r\n\r\n", size);

			u = user_list;
			while (u!=NULL) {
				if (u->status == IMAGEONCE) {
					queue_block(u, text, strlen(text));
					queue_block(u, newimg, size);
					queue_block(u, "\r\n", 2);
					u->status = DONE;
				}else
				if (u->status == IMAGESTREAM) {
					queue_block(u, text, strlen(text));
					queue_block(u, newimg, size);
					snprintf(text, 1024, "\r\n--%s\r\n", BOUNDARY);
					queue_block(u, text, strlen(text));
				}
				u=u->next;
			}
			if (web_img_fmt != V4L2_PIX_FMT_JPEG)
				free(newimg);

			free(web_img);
			web_img = NULL;
		}
	} while (1);
}

void parse_request(struct user *u)
{
	char *s, *url, *r, *n;
	char text[1024];

	if (u->inbuf_used >= u->inbuf_size) {
		snprintf(text, 1024, "HTTP/1.0 400 Bad request\r\n\r\n");
		queue_block(u, text, strlen(text));
		u->status = DONE;
		return;
	}

	/* trim off \r\n, if there isnt one were not ready */
	r=strchr(u->inbuf,'\r');
	n=strchr(u->inbuf,'\n');
	if (r==NULL && n==NULL) return;
	if (r) *r=0;
	if (n) *n=0;

	if ((s=strchr(u->inbuf,' '))==NULL) {
		snprintf(text, 1024, "HTTP/1.0 400 Bad request\r\n\r\n");
		queue_block(u, text, strlen(text));
		u->status = DONE;
		return;
	}
	*s = 0;
	s++;
	url = s;
	if ((s=strchr(url,' '))==NULL) {
		snprintf(text, 1024, "HTTP/1.0 400 Bad request\r\n\r\n");
		queue_block(u, text, strlen(text));
		u->status = DONE;
		return;
	}
	*s = 0;
	if (strcasecmp(u->inbuf,"GET")==0) {
		if (strcasecmp(url, "/")==0 || strcasecmp(url, "/once")==0) {
			snprintf(text, 1024, "HTTP/1.0 200 Okay\r\n");
			u->status = IMAGEONCE;
		} else
		if (strcasecmp(url, "/stream")==0) {
			snprintf(text,sizeof(text),"HTTP/1.0 200 Okay\r\n%sContent-type: multipart/x-mixed-replace; boundary=%s\r\n\r\n--%s\r\n", SERVER, BOUNDARY, BOUNDARY);
			u->status = IMAGESTREAM;
		} else {
			snprintf(text, 1024, "HTTP/1.0 200 Okay\r\nContent-type: text/plain\r\n\r\nURL: %s\r\n", url);
			u->status = DONE;
		}
		queue_block(u, text, strlen(text));
		return;
	}

	snprintf(text, 1024, "HTTP/1.0 405 Unsupported method\r\n\r\n");
	queue_block(u, text, strlen(text));
	u->status = DONE;
	return;

}

void queue_web(unsigned char *img, unsigned int fmt, int size)
{
	if (web_img == NULL && user_list != NULL) {
		unsigned char *new;
		new = malloc(size);
		memcpy(new, img, size);
		web_img_size = size;
		web_img_fmt = fmt;
		web_img = new;
	}
}
