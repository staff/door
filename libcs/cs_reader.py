# Contactless Smartcard Reader Library
# Copyright (C) 2007 Steve Hill <steve@nexusuk.org>
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import time
import struct
import cs_constants
import cs_card

class reader:
	def __init__(self, bus, nodeid):
		print("Instantiated reader with id %02x" % nodeid)
		self.bus = bus
		self.nodeid = nodeid
		self.rxframes = []

	def send_command(self, command, payload=""):
		"""Send a command to the reader."""
		return self.bus.send_command(command, payload, self.nodeid)

	def frame_from_bus(self, frame):
		"""Called by the bus object when a frame is received."""
		self.rxframes.append(frame)

	def read_response(self, end_time=None):
		"""Read a pending responses.  If end_time is not None wait until then for response to arrive."""
		if (len(self.rxframes) > 0):
			return self.rxframes.pop(0)
		if (end_time == None):
			return None
		while (time.time() < end_time):
			self.bus.read_response(end_time, self.nodeid)
			if (len(self.rxframes) > 0):
				return self.rxframes.pop(0)
		return None

	def send_and_wait(self, command, payload="", timeout = cs_constants.DEFAULT_TIMEOUT, retries=3):
		"""Send a command to the reader and wait for a response."""
		lastframe = None
		while (retries >= 0):
			retries -= 1
			end_time = time.time() + timeout
			self.send_command(command, payload)
			while (time.time() < end_time):
				frame = self.read_response(end_time)
				if (frame['command'] == cs_constants.CS_MIFARE_ANTICOLLISION):
					lastframe = frame
				if (frame['status'] == 0):
					return frame
		return frame

	def discover_cards(self, end_time = None):
		"""Look for any cards in range and return a dictionary of card objects"""
		if (end_time == None):
			end_time = time.time() + cs_constants.DEFAULT_TIMEOUT
		cards = {}
		while (time.time() < end_time):
			self.send_command(cs_constants.CS_MIFARE_ANTICOLLISION, "")
			while (time.time() < end_time):
				frame = self.read_response(end_time)
				if ((frame == None) or (frame['status'] != 0)):
					# Something broke - retransmit
					break
				if (frame['command'] != cs_constants.CS_MIFARE_ANTICOLLISION):
					# Keep listening
					continue
				if (len(frame['payload']) < 4):
					print("Short frame")
					# Something broke - retransmit
					break
				cardid = struct.unpack(">I", frame['payload'])[0]
				if (not cards.has_key(cardid)):
					cards[cardid] = cs_card.card(self, cardid)
		return cards
	
	def set_antenna_status(self, status):
		"""'status' is boolean.  Returns true on success"""
		if (status):
			payload = '\x01'
		else:
			payload = '\x00'
		frame = self.send_and_wait(cs_constants.CS_SET_ANTENNA_STATUS, payload)
		if ((frame != None) and (frame['status'] == 0)):
			return True
		return False

	
