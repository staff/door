# Contactless Smartcard Reader Library
# Copyright (C) 2007 Steve Hill <steve@nexusuk.org>
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

DEFAULT_TIMEOUT = 0.1

CS_SET_BAUD_RATE =		0x0101
CS_SET_DEVICE_NODE_ID =		0x0102
CS_READ_DEVICE_NODE_ID =	0x0103
CS_READ_DEVICE_MODE =		0x0104
CS_SET_BUZZER_BEEP =		0x0106
CS_SET_LED_COLOUR =		0x0107
CS_SET_READER_WORKING_STATUS =	0x0108
CS_SET_ANTENNA_STATUS =		0x010c
CS_MIFARE_REQUEST =		0x0201
CS_MIFARE_ANTICOLLISION =	0x0202
CS_MIFARE_SELECT =		0x0203
CS_MIFARE_HALT =		0x0204
CS_MIFARE_AUTH1 =		0x0206
CS_MIFARE_AUTH2 =		0x0207
CS_MIFARE_READ =		0x0208
CS_MIFARE_WRITE =		0x0209
CS_MIFARE_INITVAL =		0x020a
CS_MIFARE_READ_BALANCE =	0x020b
CS_MIFARE_DECREMENT =		0x020c
CS_MIFARE_INCREMENT =		0x020d
CS_MIFARE_RESTORE =		0x020e
CS_MIFARE_TRANSFER =		0x020f
CS_MIFARE_ULTRALIGHT_ANTICOLL =	0x0212
CS_MIFARE_ULTRALIGHT_WRITE =	0x0213
CS_WRITE_KEY_STORE =		0x0216


