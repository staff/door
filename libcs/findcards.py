#!/bin/env python

# Contactless Smartcard Reader Libary Example
# Copyright (C) 2007 Steve Hill <steve@nexusuk.org>
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import sys
import cs_bus

bus = cs_bus.bus_sock("bus")
bus.discover_readers()
if (len(bus.readers) <= 0):
	print("No readers found")

for r in bus.readers.values():
	# Turn on the antenna
	r.set_antenna_status(True)

lastcards = {}

while True:
	cards = {}
	for r in bus.readers.values():
		cards.update(r.discover_cards())
		for c in cards.values():
			if (not c.cardid in lastcards.keys()):
				print("Card appeared: 0x%08x" % c.cardid)
	for c in lastcards.values():
		if (not c.cardid in cards.keys()):
			print("Card vanished: 0x%08x" % c.cardid)
	lastcards = cards



