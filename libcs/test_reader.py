#!/bin/env python

# Contactless Smartcard Reader Emulator
# Copyright (C) 2007 Steve Hill <steve@nexusuk.org>
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import cs_bus
import cs_constants
import struct
import time
import socket
import os

class bus_testreaders(cs_bus.bus_sock):
	def __init__(self, path):
		self.send_response = self.send_command
		self.read_command = self.read_response
		try:
			os.unlink(path)
		except:
			pass
		self.listensock = socket.socket(socket.AF_UNIX)
		self.listensock.bind(path)
		self.listensock.listen(1)
		print("Waiting for connection")
		(self.sock,self.sockaddr) = self.listensock.accept()
		print("Ok")
		cs_bus.bus.__init__(self)
	
	def handle_frame(self, frame):
		"""Handle a received frame.  "frame" excludes start magic and length."""
		if (len(frame) < 5):
			print("Frame too short")
			return None

		checksum = self.checksum(frame[:len(frame)-1])
		rx_checksum = ord(frame[len(frame)-1])
		if (checksum != rx_checksum):
			print("Checksum error in received frame, calculated %02x." % checksum + " received %02x.  Discarding" % rx_checksum)
			return None

		rx_nodeid = struct.unpack("<H", frame[0:2])[0]
		data = {
			'nodeid':	rx_nodeid,
			'command':	struct.unpack("<H", frame[2:4])[0],
			'payload':	frame[4:len(frame)-1]
		}
		if (rx_nodeid == 0x0000):
			for r in self.readers.values():
				r.frame_from_bus(data)
		elif (self.readers.has_key(rx_nodeid)):
			self.readers[rx_nodeid].frame_from_bus(data)

		return data
	
	def send_command(self, command, status, payload, nodeid):
		cs_bus.bus_sock.send_command(self, command, struct.pack("<B", status) + payload, nodeid)


class testreader:
	def __init__(self, bus, nodeid):
		self.bus = bus
		self.nodeid = nodeid
		self.cards = {}
		self.selected = None

	def frame_from_bus(self, frame):
		if (frame['command'] == cs_constants.CS_SET_BAUD_RATE):
			self.bus.send_response(frame['command'], 0x00, "", self.nodeid)
		elif (frame['command'] == cs_constants.CS_READ_DEVICE_MODE):
			self.bus.send_response(frame['command'], 0x00, "\x43\x52\x35\x30\x30\x4c\x52\x2d\x31\x32\x30\x33", self.nodeid)
		elif (frame['command'] == cs_constants.CS_SET_BUZZER_BEEP):
			self.bus.send_response(frame['command'], 0x00, "", self.nodeid)
		elif (frame['command'] == cs_constants.CS_SET_LED_COLOUR):
			self.bus.send_response(frame['command'], 0x00, "", self.nodeid)
		elif (frame['command'] == cs_constants.CS_SET_ANTENNA_STATUS):
			self.bus.send_response(frame['command'], 0x00, "", self.nodeid)
		elif (frame['command'] == cs_constants.CS_MIFARE_REQUEST):
			# This is probably all wrong
			if (frame['payload'] == '\x52'):	# Request all
				self.bus.send_response(frame['command'], 0x00, "\x04\x00", self.nodeid)
			if (frame['payload'] == '\x26'):	# Request idle
				self.bus.send_response(frame['command'], 0x00, "\x04\x00", self.nodeid)
		elif (frame['command'] == cs_constants.CS_MIFARE_ANTICOLLISION):
			for (card) in self.cards.values():
				self.bus.send_response(frame['command'], 0x00, card.id, self.nodeid)
		elif (frame['command'] == cs_constants.CS_MIFARE_SELECT):
			if (self.cards.has_key(frame['payload'])):
				self.cards(frame['payload']).rx_select(self)
				self.selected = frame['payload']
			else:
				self.selected = None
				self.bus.send_response(frame['command'], 0x01, "", self.nodeid)
		elif (frame['command'] == cs_constants.CS_MIFARE_HALT):
			if (self.cards.has_key(self.selected)):
				self.bus.send_response(frame['command'], 0x00, "", self.nodeid)
			else:
				self.bus.send_response(frame['command'], 0x01, "", self.nodeid)
		# Lots of missing functionality :)
	
	def add_card(self, card):
		self.cards[card.id] = card

class card:
	def __init__(self, id):
		self.id = id
	
	def rx_select(self, reader):
		reader.bus.send_response(frame['command'], 0x00, "\x88", reader.nodeid) # What is the returned payload?

bus = bus_testreaders("bus")

bus.add_reader(testreader(bus, 0x1234))
bus.add_reader(testreader(bus, 0x5678))

bus.readers[0x1234].add_card(card('\x54\x32\x52\x93'))
bus.readers[0x1234].add_card(card('\x36\x13\xb3\xc8'))

while (True):
	bus.read_command(time.time() + 100000)

