#!/bin/env python

# Contactless Smartcard Reader Library
# Copyright (C) 2007 Steve Hill <steve@nexusuk.org>
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import struct
import time
import cs_reader
import cs_constants
import socket
import select

START_MAGIC = 0xaabb

def hexprint(buf,prefix=""):
	return
	print prefix,
	for octet in buf:
		print '%02x' % ord(octet),
	print('')

class bus:
	"""Base class for interfacing with a bus connected to contactless smart card readers."""
	def __init__(self):
		self.readers = {}
		self.rxstate = 0
		self.unstuff_next = False
	
	def add_reader(self, reader):
		self.readers[reader.nodeid] = reader
	
	def checksum(self, buf):
		"""Calculate the checksum of a buffer."""
		sum = 0
		for octet in buf:
			sum ^= ord(octet)
		return sum

	def stuff(self, buf):
		"""Stuff 0x00 after 0xaa"""
		out = ""
		for octet in buf:
			out += octet
			if (ord(octet) == 0xaa):
				out += '\x00'
		return out
	
	def write_frame(self, frame):
		"""Send the frame onto the bus"""
		"""Dummy function - replace me"""
		hexprint(frame, "=> ")
	
	def read_octet(self, until):
		"""Read an octet from the bus"""
		"""Dummy function - replace me"""
		timeout = until - time.time()
		if (timeout <= 0):
			return None
		time.sleep(timeout)
		return None

	def send_command(self, command, payload="", nodeid=0x0000):
		"""Send a command onto the bus"""
		buf = struct.pack("<H", nodeid)
		buf += struct.pack("<H", command)
		buf += payload
		
		lenbuf = struct.pack("<H", len(payload) + 5)	# node id + command + payload + checksum

		frame = struct.pack(">H", START_MAGIC)
		frame += self.stuff(lenbuf + buf)
		frame += struct.pack("<B", self.checksum(buf))	# Should we checksum after stuffing instead?  who knows!
		self.write_frame(frame)

	def handle_frame(self, frame):
		"""Handle a received frame.  "frame" excludes start magic and length."""
		if (len(frame) < 6):
			print("Frame too short")
			return None

		checksum = self.checksum(frame[:len(frame)-1])
		rx_checksum = ord(frame[len(frame)-1])
		if (checksum != rx_checksum):
			print("Checksum error in received frame, calculated %02x." % checksum + " received %02x.  Discarding" % rx_checksum)
			return None

		rx_nodeid = struct.unpack("<H", frame[0:2])[0]
		data = {
			'nodeid':	rx_nodeid,
			'command':	struct.unpack("<H", frame[2:4])[0],
			'status':	struct.unpack("<B", frame[4:5])[0],
			'payload':	frame[5:len(frame)-1]
		}
		if (self.readers.has_key(rx_nodeid)):
			self.readers[rx_nodeid].frame_from_bus(data)

		return data

	def discover_readers(self, timeout=cs_constants.DEFAULT_TIMEOUT, maxdevices=None, tries=4):
		"""Broadcast onto the bus to discover all readers."""
		"""If maxdevices is defined, the search will end once this number of responses have been seen."""
		while (tries > 0):
			tries -= 1
			self.send_command(cs_constants.CS_READ_DEVICE_MODE)
			end_time = time.time() + timeout
			while (time.time() < end_time):
				frame = self.read_response(end_time)
				if (frame != None):
					if (not self.readers.has_key(frame['nodeid'])):
						self.add_reader(cs_reader.reader(self, frame['nodeid']))

	def read_response(self, end_time, nodeid=None):
		"""Read a response from the bus.  If a nodeid is specified returned results will be filtered for this ID."""
		"""Any data received will also be handed to all of the readers' frame_from_bus functions."""
		while (time.time() < end_time):
			octet = self.read_octet(end_time)
			if (octet == None):
				return None	# Read returned nothing - give up for now
			if (self.unstuff_next):
				self.unstuff_next = False
				if (octet != '\x00'):
					print("Expected stuffed null, discarded 0x%02x" % ord(octet))
					self.rxstate = 0
				continue
			# Begin finite state machine
			if (self.rxstate == 0):
				if (ord(octet) == (START_MAGIC >> 8)):
					self.rxstate += 1
				else:
					print("Expected first octet of magic number, discarded 0x%02x" % ord(octet))
					self.rxstate = 0
			elif (self.rxstate == 1):
				if (ord(octet) == (START_MAGIC & 0xff)):
					self.rxstate += 1
					self.rxbuf = ""
				else:
					print("Expected second octet of magic number, discarded 0x%02x" % ord(octet))
					self.rxstate = 0
			else:
				if (octet == '\xaa'):
					self.unstuff_next = True
				if (self.rxstate == 2):		# Length LSB
					self.rxlength = ord(octet)
					self.rxstate += 1
				elif (self.rxstate == 3):	# Length MSB
					self.rxlength |= ord(octet) << 8
					if (self.rxlength < 1):
						print("Frame too short - reported length was " + str(self.rxlength))
						self.rxstate = 0
					else:
						self.rxstate += 1
				else:	# fill rx buffer with remaining data
					self.rxbuf += octet
					self.rxlength -= 1
					if (self.rxlength <= 0):
						hexprint(self.rxbuf, "<=  .. .. .. ..")
						frame = self.handle_frame(self.rxbuf)
						self.rxstate = 0	# Reset FSM for next frame
						if (frame != None):
							if ((nodeid == None) or (nodeid == frame['nodeid'])):
								return frame
		return None
	
class bus_sock(bus):
	def __init__(self, path):
		self.sock = socket.socket(socket.AF_UNIX)
		self.sock.connect(path)
		bus.__init__(self)
	
	def write_frame(self, frame):
		hexprint(frame, "=> ")
		self.sock.send(frame)
	
	def read_octet(self, until):
		timeout = until - time.time()
		if (timeout < 0):
			timeout = 0
		ready,_,_ = select.select([self.sock],[],[], timeout)
		if (not ready):
			return None
		octet = self.sock.recv(1)
		if (len(octet) != 1):
			return None
		return octet


if __name__=="__main__":
	b = bus_sock("bus")
	b.discover_readers()
	if (len(b.readers) <= 0):
		print("No readers found")
	while True:
		for r in b.readers.values():
			cards = r.discover_cards()
			print("%i cards found:" % len(cards))
			for c in cards.values():
				print("\t%08x" % c.cardid)

