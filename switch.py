#!/usr/bin/env python
import time
import sys
import syslog
import os
import serial
import _doorlock as doorlock
import gui
import traceback
import threading
import pygame
import httplib
import socket

STARTPAT = "%N?;"
ENDPAT = "?+N?"
FAILPAT = STARTPAT+"F"+ENDPAT
FPS = 4 # fps of the spinner, and how many serial port
        # timeouts per second. Above 32 and you start to get card errors
DEBUG = False 
serial_port_swipe = None
serial_port_rfid = None
screenlock = None
swipethread = None
rfidthread = None

def hex2(n):
	return '%02x' % n

# these two classes, readRFID and readSwipe could probably go in
# seperate .py files, I guess. There's also some amount of code 
# re-use, so it could be made better. Possible have an extra
# class that controls the unlocking of the door and display of 
# stuff on the screen, that readRFID and readSwipe could just report to
class readRFID(threading.Thread):
	def __init__(self, serial_port_rfid, screenlock):
		threading.Thread.__init__(self)
		self.serial_port_rfid = serial_port_rfid
		self.screenlock = screenlock
		self.halt = 0
		syslog.syslog(syslog.LOG_INFO, "Door - RFID thread started.")
		
	def stopnow(self):
		self.half = 1

	def run(self):  # called when the thread is .start()ed.
		idlecount = 0
		while (not self.halt):
			serial_port_rfid.write('\xAA\xBB\x06\x00\x00\x00\x01\x02\x52\x51')
			rfidrawdata = serial_port_rfid.read(12) # Get card type
			
			if (len(rfidrawdata) == 10): # Didn't read anything this time
				idlecount += 1
				
				self.screenlock.acquire() # acquire the screenlock so nothing else
				screen.Idle()			  # can write to the screen
				self.screenlock.release()
				
				# Re-read the cards file every ~15 seconds
				#if idlecount == (15*FPS):
				#	ReadCardsFile()
				#	idlecount = 0
				continue
			else:
				# hooray, got something. Now let's work out what it is
				serial_port_rfid.write('\xAA\xBB\x06\x00\x00\x00\x02\x02\x04\x04')
				cardnum = serial_port_rfid.read(14) # Get card number
				hexcardnum = "".join(map(hex2, map(ord, cardnum[9:-1])))	
				data = map(hex,map(ord,cardnum[-5:-1]))
				data = hexDataToNumber(data) # Convert it to a nice decimal formal
				if (data == '182210'):
					self.screenlock.acquire()
					screen.ChangeStatus("Bad card read - did you swipe two cards?", (0,0,0), (255, 64, 64), screen.warn_icon)
					time.sleep(3)
					self.screenlock.release()
					continue
				if not DEBUG:
					syslog.syslog(syslog.LOG_INFO, "Card data %s" % data)
					
				cardtype = "RFID Card"
				
				if (Cards.has_key(data) or HexCards.has_key(hexcardnum)):	# Check we know the card number
					screen.Log("%s - Unlocked for %s with %s" % (time.ctime(), Cards[data], cardtype))
					hexynum = "".join(map(hex2, map(ord, cardnum)))
					hexcardsfile = open("hexcards.txt","a")
					hexcardsfile.write("%s:%s" % (Cards[data], hexynum))
					hexcardsfile.close()

					if not DEBUG:
						syslog.syslog(syslog.LOG_INFO, "%s with %s" % (Cards[data], cardtype))

					self.screenlock.acquire()	# Acquire the screenlock so nothing else can
					OpenDoor(5)					# use the screen or door lock until we've finished
					self.screenlock.release()

				else:
					self.screenlock.acquire()
					screen.ChangeStatus("Unrecognised card number. %s" % data, (0,0,0), (255, 64, 64), screen.warn_icon)
					time.sleep(3)	# Sleep needs to be in the screenlock otherwise other methods
					self.screenlock.release() # grab the screen and the message isn't displayed

					writeLastCardToFile(data) # Write unrecognised card to file, to make it easier
											  # to stick in the database if necessary
				conn = httplib.HTTPConnection('silver')
				try:
					conn.request('GET',"http://sucs.org/doorknock.php?id=%s" % data)
					conn.close()
				except:
					conn.close()

class readSwipe(threading.Thread):
	def __init__(self, serial_port_swipe, screenlock):
		threading.Thread.__init__(self)
		self.serial_port_swipe = serial_port_swipe
		self.screenlock = screenlock
		self.halt = 0
                syslog.syslog(syslog.LOG_INFO, "Door - Swipe thread started.")

	def stopnow(self):
		self.halt = 1

	def run(self):
		idlecount = 0
		while (not self.halt):
			for event in pygame.event.get():
				if event.type == pygame.QUIT:
					Cleanup()
					sys.exit()
				elif event.type == pygame.KEYDOWN:
					if event.key == pygame.K_UP:
						Cleanup()
						sys.exit()
			swiperawdata = serial_port_swipe.read((9600/FPS)) # Check for a card
			
			if swiperawdata == "" or swiperawdata == "PORTPOWER READER":
				# Didn't read anything in before the timeout
				idlecount += 1
				
				self.screenlock.acquire() # Acquire the screenlock so nothing else
				screen.Idle()			  # can write to the screen
				self.screenlock.release()

				# Re-read the cards file every ~15 seconds
				if idlecount == (15*FPS):
					ReadCardsFile()
					idlecount = 0
				continue
			elif swiperawdata == FAILPAT: # If the card data matches the fail pattern
				self.screenlock.acquire()
				screen.ChangeStatus("Failed card read. Try again.", (0,0,0), (255, 96, 96))
				time.sleep(0.5)
				self.screenlock.release()
				continue
			else:
				# hooray, got something. Now let's work out what it is
				if not DEBUG:
					syslog.syslog(syslog.LOG_INFO, "Card data %s" % swiperawdata)
				# Get the info we want
				data = swiperawdata.replace(STARTPAT, "").replace(ENDPAT, "")
				cardtype = GetCardType(data)

				if cardtype == "Student Union Card" and data[-2:] == "00":
					data = data[:-2] # strip off the trailing zeroes
					
				if Cards.has_key(data): # Check we know the card number
					screen.Log("%s - Unlocked for %s with %s" % (time.ctime(), Cards[data], cardtype))
					if not DEBUG:
						syslog.syslog(syslog.LOG_INFO, "%s with %s" % (Cards[data], cardtype))
						
					self.screenlock.acquire()
					OpenDoor(5)
					self.screenlock.release()
				else:
					self.screenlock.acquire()
					screen.ChangeStatus("Unrecognised card number. %s" % data, (0,0,0), (255, 64, 64), screen.warn_icon)
					time.sleep(3)
					self.screenlock.release()
					
					writeLastCardToFile(data) # Write to file for easier database entry
				conn = httplib.HTTPConnection('silver')
				try:
					conn.request('GET',"http://sucs.org/doorknock.php?id=%s" % data)
					conn.close()
				except:
					conn.close()


STATUS_SOCK_PATH="status.sock"
class remoteStatus(threading.Thread):
	"""Thread allowing the status line to be updated remotely."""
	def __init__(self, screenlock):
		threading.Thread.__init__(self)
		self.screenlock = screenlock
		self.halt = 0
		try:
			os.unlink(STATUS_SOCK_PATH)
		except:
			pass
		self.sock = socket.socket(socket.AF_UNIX, socket.SOCK_DGRAM)
		self.sock.bind(STATUS_SOCK_PATH)
		os.chmod(STATUS_SOCK_PATH, 0777)
                syslog.syslog(syslog.LOG_INFO, "Door - Remote status thread started.")

	def stopnow(self):
		self.halt = 1
		os.unlink(STATUS_SOCK_PATH)

	def parse_colour(self, data):
		values = data.split(',')
		return (int(values[0]), int(values[1]), int(values[2]))

	def run(self):
		idlecount = 0
		while (not self.halt):
			data = self.sock.recv(1024)
			lines = data.split('\n')
			if (len(lines) >= 4):
				try:
					timeout = int(lines[0])
				except:
					continue
				text = lines[1]
				try:
					foreground = self.parse_colour(lines[2])
				except:
					continue
				try:
					background = self.parse_colour(lines[3])
				except:
					continue
				try:
					unlock = int(lines[4])
				except:
					continue

				self.screenlock.acquire()
				screen.ChangeStatus(text, foreground, background)
				time.sleep(timeout)
				if unlock==1:
					OpenDoor(5)
					screen.Log("%s - %s" % (time.ctime(), text))
					syslog.syslog(syslog.LOG_INFO, "Door - %s" % text)
				
				syslog.syslog(syslog.LOG_INFO, "Info received - "+lines[0]+","+lines[1]+","+lines[2]+","+lines[3]+","+lines[4])
				self.screenlock.release()
				

def OpenDoor(timeout):
	"""Unlock door for timeout seconds, and update screen"""
	if not DEBUG:
		doorlock.unlock()
	screen.ChangeBorder("unlocked")
	for i in range(timeout, 0, -1):
		screen.ChangeStatus("Door unlocked... %d" % i, (0,0,0), (128, 255, 128), screen.open_icon)
		time.sleep(1)
	screen.ChangeBorder("locked")
	#screen.ChangeStatus("Door locked.", (0,0,0), (255,128,128))
	if not DEBUG:
		doorlock.lock()


def MainLoop():
	"""Wait for a card to be swiped"""
        syslog.syslog(syslog.LOG_INFO, "Door started.")	
	#screenlock = threading.Lock() 	# Create a lock so that the threads don't fight
									# for access to the screen
	swipethread.start()
	rfidthread.start()
	remotestatusthread.start()
	swipethread.join()
	rfidthread.join()
	remotestatusthread.join()
		
def GetCardType(data):
	for length in range(1, len(data)+1):
		if CardTypes.has_key(data[0:length]):
			return CardTypes[data[0:length]]
	return "Unknown card type"
		
def Startup():
	if not DEBUG:
		# are we root?
		if not os.getuid() == 0:
			print "You're not root!"
			sys.exit(1)
		# open parallel port
		if not doorlock.openport():
			print "Couldn't open the parallel port. Do you have root permissions?"
			sys.exit(1)
		doorlock.lock()
		# set up syslog
		syslog.openlog("switch.py", syslog.LOG_PID, syslog.LOG_USER)
		# make a PID file
		pidfile = open("/var/run/switch.pid", "w")
		pidfile.write("%s" % os.getpid())
		pidfile.close()
	# open serial port
	global serial_port_rfid
	serial_port_rfid = serial.Serial(1, timeout=(1.0/FPS), baudrate=19200)
	global serial_port_swipe
	serial_port_swipe = serial.Serial(0, timeout=(1.0/FPS))
	ReadCardsFile()
	ReadCardTypesFile()
	InitialiseRFID() # Send the appropriate data to the RFID reader
	global screenlock
	screenlock = threading.Lock()
	global swipethread
	swipethread = readSwipe(serial_port_swipe, screenlock)
	global rfidthread
        rfidthread = readRFID(serial_port_rfid, screenlock)
	global remotestatusthread
	remotestatusthread = remoteStatus(screenlock)
	global screen
	screen = gui.GUI()

# Needs seeing to - currently, if the program crashes or is killed, 
# the system needs restarting. Need a way to cleanly exit.
# Possibly threads messing it up?
def Cleanup():
	global swipethread
	global rfidthread
	global serial_port_rfid
	global serial_port_swipe
	swipethread.stopnow()
	rfidthread.stopnow()
	remotestatusthread.stopnow()
	if not DEBUG:
		doorlock.lock() # lock the door if we're closing down
		if not doorlock.closeport():
			print "Couldn't close the parallel port."
		syslog.closelog()
		os.remove("/var/run/switch.pid")
	serial_port_rfid.close()
	serial_port_swipe.close()

def ReadCardsFile():
	global Cards
	global HexCards
	Cards = {}
	HexCards = {}
	lines = open("/home/admin/doorcode/cards", "r").readlines()
	# syslog.syslog(syslog.LOG_INFO, "Door - Reading cards file.")     
	for line in lines:
		if line[0] == '#':
			continue
		line = line.strip()
		try:
			card, user = line.split()[:2]
			Cards[card] = user
		except: # don't crap out when reading a dodgy line
			pass
        lines = open("hexcards", "r").readlines()
        for line in lines:
                if line[0] == '#':
                        continue
                line = line.strip()
                try:
                        card, user = line.split()[:2]
                        HexCards[card] = user
                except: # don't crap out when reading a dodgy line
                        pass


def ReadCardTypesFile():
	global CardTypes
	CardTypes = {}
	lines = open("/home/admin/doorcode/cardtype", "r").readlines()
	# syslog.syslog(syslog.LOG_INFO, "Door - Reading cardtypes file.")     
	for line in lines:
		if line[0] == '#':
			continue
		line = line.strip()
		try:
			chopchop = line.split()
			prefix = chopchop[0]
			cardtype = " ".join(chopchop[1:])
			CardTypes[prefix] = cardtype
		except: #don't crap out when reading a dodgy line
			pass 

def InitialiseRFID():
	# rf_antenna_sta (disable antenna)
	serial_port_rfid.write('\xAA\xBB\x06\x00\x00\x00\x0C\x01\x00\x0D')
	serial_port_rfid.read(10)
	# rf_init_type (set card protocol)
	serial_port_rfid.write('\xAA\xBB\x06\x00\x00\x00\x08\x01\x41\x48')
	serial_port_rfid.read(10)
	# rf_antenna_sta (enable antenna)
	serial_port_rfid.write('\xAA\xBB\x06\x00\x00\x00\x0C\x01\x01\x0C')
	serial_port_rfid.read(10)

def writeLastCardToFile(data):
	lastcardfile = open("/home/admin/lastcard", "w")
	lastcardfile.write(data)
	lastcardfile.close()

def hexDataToNumber(hexdata): # Convert the hex data from an RFID read to a nice decimal number
	# I expect there's a nicer way of doing this with some kind of map or list comprehension stuffs.
	return `int(hexdata[0], 16)` + `int(hexdata[1], 16)` + `int(hexdata[2], 16)` + `int(hexdata[3], 16)`
		
if __name__=="__main__":
	try:
		Startup()
		MainLoop()
	except Exception, e:
		# uh-oh, something went wrong
		# fire off an e-mail and try to lock the door before closing down
		f = open("error.log", "a")
		traceback.print_exc(file=f)
		f.close()
		try:
			Cleanup()
			sys.exit()
		except:
			pass

		#try:
			#import smtplib
			#server = smtplib.SMTP("localhost")
			#server.sendmail("door@door.sucs.org", "%s@sucs.org"%"admin", "From: The Door <door@sucs.org\r\nTo: SUCS Admin <%s@sucs.org>\r\nSubject: Door broke, attention required\r\n\r\nHello!\nI, your faithful servant the door software, caught a fatal exception and I've crashed out. If it's any use, the exception message was:\n%s\nIn my final agonising moments, the lock was signalled to lock, but this may not have actually locked the door.\nThe first thing to do is poke Dave A (if he's online) because this may be his fault, or he may know about it.\nIf not, you could check the processes on door to see I'm still running. Look for a line that ends with something like 'python ./switch.py'. Kill off this process and try running '/etc/init.d/switch start' to bring me back to life.\n\nHope everything works out for the best, love\nDoor software" % ("admin", e))
			#server.quit()
		#except:
			# we're in trouble now, if the e-mail didn't send...
			#pass
#	Cleanup()
