#!/usr/bin/env python
import sys
import time
import os
import serial

global serial_port

serial_port = serial.Serial(0, timeout=1, baudrate=19200)

serial_port.write('\xAA\xBB\x06\x00\x00\x00\x0C\x01\x00\x0D')
serial_port.read(10)
serial_port.write('\xAA\xBB\x06\x00\x00\x00\x08\x01\x41\x48')
serial_port.read(10)
serial_port.write('\xAA\xBB\x06\x00\x00\x00\x0C\x01\x01\x0C')
serial_port.read(10)

while 1:
	serial_port.write('\xAA\xBB\x06\x00\x00\x00\x01\x02\x52\x51')
	request = serial_port.read(12)
	if (len(request) == 10):
		print "no card"
	else:
		serial_port.write('\xAA\xBB\x06\x00\x00\x00\x02\x02\x04\x04')
		cardnum = serial_port.read(14)
		print "Card with id %s" % map(hex,map(ord,cardnum[-5:-1]))
	time.sleep(1)

