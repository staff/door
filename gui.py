import pygame
import time
import sys

class GUI:
	def __init__(self):
		pygame.init()
		pygame.mouse.set_visible(False)
		self.screen = pygame.display.set_mode((800, 600),pygame.FULLSCREEN)
		self.background = pygame.image.load("images/background.png")
		self.screen.blit(self.background, (0,0))
		self.log = ['', '', '', '', '', ''] # can change length here
		self.InitSpinner()
		self.open_icon = pygame.image.load("images/open.png")
		self.warn_icon = pygame.image.load("images/warning.png")
		self.wait_icon = pygame.image.load("images/wait.png")
		self.ChangeBorder("locked")
		self.Idle()
		pygame.display.update()

	def InitSpinner(self):
		self.frames = []
		self.framecount = 8
		self.frame = 0
		for i in range(self.framecount):
			self.frames.append(pygame.image.load("images/spin-frame%d.png" % i))
		
	def ChangeStatus(self, text, fontcolour=(0,0,0), bgcolour=(255,255,255), icon=None):
		self.screen.fill(bgcolour, (15, 552, 771, 34))
		self.screen.blit(
			pygame.font.Font(None, 24).render(
				text,
				True, #antialias
				fontcolour
			),
			(42,565)
		)
		if icon:
			self.screen.blit(icon, (20, 567))
		pygame.display.update()

	def Log(self, entry):
		self.log.append(entry)
		self.log.remove(self.log[0])
		self.DisplayLog()
	
	def DisplayLog(self):
		self.screen.fill((255, 255, 255), (19, 425, 780, 120))
		count = 0
		for entry in self.log:
			self.screen.blit(
				pygame.font.Font(None, 24).render(
					entry,
					True,
					(0,0,0)
				),
				(19, 450 + (16 * count))
			)
			count += 1
	
	def ChangeBorder(self, state):
		"""Draw a red or green border around outside of
		   the screen to indicate lock state."""
		if state == "locked":
			colour = (255,0,0)
		else:
			colour = (0,255,0)
		
		width = 3
		
		self.screen.fill(colour, (0,0,800,width)) # top
		self.screen.fill(colour, (0,0,width,600)) # left
		self.screen.fill(colour, (0,600-width,800,width)) # bottom
		self.screen.fill(colour, (800-width,width,width,600-width)) #right
	
	def Idle(self):
		self.ChangeStatus("Waiting for card...", (0,0,0), (192, 192, 255), self.frames[self.frame])
									#(247, 192, 42) <- sickly yellow
		self.frame += 1
		if self.frame == 8:
			self.frame = 0
		
		# check if there's any input from the keyboard
		#for event in pygame.event.get((pygame.KEYDOWN, pygame.QUIT)):
		#	if event.type == pygame.QUIT or (event.type == pygame.KEYDOWN and event.key == pygame.K_ESCAPE):
		#		sys.exit()
	

if __name__ == "__main__":
	gui = GUI()
	#gui.CreateScreen()
	blue = 0
	while True:
		gui.ChangeStatus("Waiting for card...", (0,0,blue,255))
		time.sleep(0.05)
		blue += 15
		if blue > 255:
			blue = 0
	gui.ChangeStatus("10/08/2005 18:06: stringfellow with Student Union Card")
	time.sleep(5)
