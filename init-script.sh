#!/bin/bash
#lsmod | grep i810fb >/dev/null
#loaded=$?
#if [ $loaded = 1 ]; then
#	modprobe i810fb vram=8 bpp=32
#fi

case "$1" in
start)
	X 2> /dev/null &
	DISPLAY=:0 screen -dmS switch ~admin/doorcode/switchlaunch.sh
	xset -display :0 s off
        xset -display :0 -dpms
	;;
stop)
	kill `cat /var/run/switch.pid`
	kill `pidof X`
	;;
*)
	echo "Usage: $0 {start|stop}"
	;;
esac
