#!/usr/bin/env python

import socket
import sys

STATUS_SOCK_PATH="/home/admin/doorcode/status.sock"

args = sys.argv
progname = args.pop(0)

if (len(args) < 4):
	print("Usage: "+progname+" <timeout> <message> <fg red>,<fg green>,<fg blue> <bg red>,<bg green>,<bg blue> <unlock>")
	sys.exit(1)

timeout = args.pop(0)
text = args.pop(0)
fg = args.pop(0)
bg = args.pop(0)
unlock = args.pop(0)

sock = socket.socket(socket.AF_UNIX, socket.SOCK_DGRAM)
sock.connect(STATUS_SOCK_PATH)
sock.send(timeout+"\n"+text+"\n"+fg+"\n"+bg+"\n"+unlock)

